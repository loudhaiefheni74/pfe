<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function client()
    {
        return $this->belongsTo(Client::class);

    }//end of user


    public function produits()
    {
        return $this->belongsToMany(Produit::class, 'produit_order')->withPivot('quantity');

    }//
}
