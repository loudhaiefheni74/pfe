<?php

namespace App\Http\Controllers;
use App\User;
use App\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
     {
        $users=User::all();
        $clients=Client::paginate(5);
        return view('home',compact('users'),compact('clients'));
    }

}
