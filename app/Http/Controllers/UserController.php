<?php

namespace App\Http\Controllers;

use App\User;
use Dotenv\Result\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use PHPUnit\Framework\Constraint\Constraint;
use Intervention\Image\Facades\Image as Image;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function myprofile()
    {
       return view('users.profile');

    }
    public function profileupdate(Request $request)
    {
         $user_id = Auth::user()->id;
         $user = User::findorFail($user_id);

         $user->name=$request->input('name');
         $user->address=$request->input('address');
         $user->city=$request->input('city');
         $user->date_naiss=$request->input('date_naiss');
         $user->phone=$request->input('phone');
         $user->alternate_phone=$request->input('alternate_phone');

         if($request->hasFile('image'))
                {
                     $destination ='uploads/user_images/'.$user->image;

                        if(File::exists($destination)){

                        File::delete($destination);
                        }

                        $file = $request->file('image');

                        $extension = $file->getclientoriginalExtension();

                        $filename = time().'.' . $extension;

                        $file->move('uploads/user_images/', $filename);

                        $user->image = $filename;
                }

        $user->Update();
        $request->session()->flash('message',$user->name.(' Updated successfully'));
        return redirect()->back()->with('status','Profile Updated');




    }







    public function index(Request $request)
    {



            $users=User::all();


        return view('users.index',compact('users'));



    }

    public function create(User $user)
    {

        return view('users.create');

    }




    public function edit(User $user)
    {

        return view('users.edit',compact('user'));

    }
    public function store(Request $request)
    {


        $request->validate([

            'name'=>'required|min:5|max:20',
            'email'=>'required',
            'password'=>'required|min:6|max:20',
            'password_confirmation'=>'required|same:password',
            'roles'=>'required|array|min:1',



        ]);

        $request_data = $request->except(['password','password_confirmation','permissions','image']);
        $request_data['password']=bcrypt($request->password);

        if ($request->image) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('uploads/user_images/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

        }//end of if


        $user =User::create($request_data);

        $user->syncRoles($request->roles);









        session()->flash('message',$user->name.(' added successfully'));
        return redirect()->route('users.index');



    }
    public function Update(Request $request,User $user)
    {

            $request->validate([

                'name'=>'required',
                'roles'=>'required|array|min:1',

            ]);

            $user->address=$request->input('address');
            $user->city=$request->input('city');
            $user->date_naiss=$request->input('date_naiss');
            $user->phone=$request->input('phone');
            $user->alternate_phone=$request->input('alternate_phone');


            $requestData = $request->except('email');
            $user->update($requestData);

            $user->syncRoles($request->roles);


                $request->session()->flash('message',$user->name.(' Updated successfully'));




            return      redirect()->route('users.index');


    }
    public function destroy(Request $request,User $user)
    {
        Storage::disk('public_uploads')->delete('produits_images/' . $user->image);
            $user->delete();
            session()->flash('message',$user->name.(' Deleted successfully'));

            return redirect()->route('users.index');

    }
    public function search()
    {
        $search_text = $_GET['query'];
        $users=User::where('name','LIKE','%'.$search_text.'%')->get();

        return view('users.search',compact('users'));



    }
    public function searchprofiles()
    {
        $search_text = $_GET['query'];
        $users=User::where('name','LIKE','%'.$search_text.'%')->get();

        return view('users.searchprofiles',compact('users'));



    }
    public function profiles()
    {
        $users=User::all();
        return view('users.profiles',compact('users'));
    }




}



/*
<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this ->middleware(['role:admin']);

    }





    public function index()
    {
        $users = User ::all();
        return view('users.index',compact('users'));

    }






    public function edit(User $user)
    {

        return view('users.edit',compact('user'));

    }
    public function Update(Request $request,User $user)
    {

            $request->validate([

                'name'=>'required',
                'roles'=>'required|array|min:1'
            ]);

            $requestData = $request->except('email');
            $user->update($requestData);
            $user->syncRoles($request->roles);
            return      redirect()->route('users.index');

    }

}
*/
