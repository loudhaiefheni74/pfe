<?php

namespace App\Http\Controllers;

use App\Category;
use App\Client;
use App\Order;
use App\Produit;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
        public function create(Client $client)
        {
            $categories = Category::with('produits')->get();
            $orders = $client->orders()->with('produits')->paginate(5);
            return view('orders.create', compact( 'client', 'categories', 'orders'));

        }//end of create

        public function store(Request $request, Client $client)
        {

            $request->validate([
                'produits' => 'required|array',


            ]);

            $this->attach_order($request, $client);

            session()->flash('message',('Added successfully'));
            return redirect()->route('orders.index');

        }//end of store

        public function edit(Client $client, Order $order)
        {
            $categories = Category::with('produits')->get();
            $orders = $client->orders()->with('produits')->paginate(5);
            return view('orders.edit', compact('client', 'order', 'categories', 'orders'));

        }//end of edit

        public function update(Request $request, Client $client, Order $order)
        {
            $request->validate([
                'produits' => 'required|array',
            ]);

            $this->detach_order($order);

            $this->attach_order($request, $client);

            session()->flash('message',(' Updated successfully'));
            return redirect()->route('orders.index');

        }//end of update

        private function attach_order($request, $client)
        {
            $order = $client->orders()->create([]);

            $order->produits()->attach($request->produits);

            $total_price = 0;

            foreach ($request->produits as $id => $quantity) {

                $produit = Produit::FindOrFail($id);
                $total_price += $produit->sale_price * $quantity['quantity'];

                $produit->update([
                    'quantity' => $produit->quantity - $quantity['quantity']
                ]);

            }//end of foreach

            $order->update([
                'total_price' => $total_price
            ]);

        }//end of attach order

        private function detach_order($order)
        {
            foreach ($order->produits as $produit) {

                $produit->update([
                    'quantity' => $produit->quantity + $produit->pivot->quantity
                ]);

            }//end of for each

            $order->delete();

        }//end of detach order

    }//end of controller
