<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderindexController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::whereHas('client', function ($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');

        })->paginate(5);

        return view('ordersindex.index', compact('orders'));

    }//end of index

    public function produits(Order $order)
    {
        $produits = $order->produits;
        return view('orders._produits', compact('order', 'produits'));

    }//end of produits

    public function destroy(Order $order)
    {
        foreach ($order->produits as $produit) {

            $produit->update([
                'quantity' => $produit->quantity + $produit->pivot->quantity
            ]);

        }//end of for each

        $order->delete();
        session()->flash('message'(' Deleted successfully'));
        return redirect()->route('ordersindex.index');

    }//end of order
}
