<?php

namespace App\Http\Controllers;

use App\Category;
use App\Client;
use App\Produit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Dotenv\Result\Success;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use PHPUnit\Framework\Constraint\Constraint;
use Intervention\Image\Facades\Image as Image;

class ProduitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {

        $categories = Category::all();

        $produits = Produit ::when($request->category_id,function($q)use($request){
            return $q->where('category_id',$request->category_id);
        })->latest()->paginate(10);


        return view('produits.index',compact('produits'),compact('categories'));
    }//end of index

    public function create()
    {
        $categories = Category:: all();
        return view('produits.create',compact('categories'));


    }//end of create


    public function store(Request $request,Produit $produit)
    {
        $rules = [
            'category_id'=>'required'
        ];
        $rules += [

            'name'=>'required|max:20',
            'purchase_price'=>'required',
            'sale_price'=>'required',
            'quantity'=>'required',

        ];



        $request->validate($rules);
        $request_data = $request->all();


        if ($request->image) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('uploads/produits_images/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

        }//end of if


        $produit =Produit::create($request_data);













        session()->flash('message',$produit->name.(' added successfully'));
        return redirect()->route('produits.index');





    }//end of store


    public function edit(Produit $produit)
    {
        $categories=Category::all();
        return view('produits.edit',compact('produit'),compact('categories'));
    }//end of edit

    public function update(Request $request , Produit $produit)
    {
        $rules = [
            'category_id'=>'required'
        ];
        $rules += [

            'name'=>'required|max:20',
            'purchase_price'=>'required',
            'sale_price'=>'required',
            'quantity'=>'required',

        ];



        $request->validate($rules);
        $request_data = $request->all();

/*
        if ($request->image) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('uploads/produits_images/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

        }//end of if

*/







        $produit->Update($request_data);




            $request->session()->flash('message',$produit->name.(' Updated successfully'));




        return      redirect()->route('produits.index');


    }//end of update




    public function destroy(Request $request,Produit $produit)
    {
 Storage::disk('public_uploads')->delete('produits_images/' . $produit->image);

            $produit->delete();
            session()->flash('message',$produit->name.(' Deleted successfully'));

            return redirect()->route('produits.index');

    }

    public function searchProduits(Request $request
    )
    {
        $categories = Category::all();

        $search_text = $_GET['query'];
        $produits=Produit::where('name','LIKE','%'.$search_text.'%')->latest()->paginate(1);


        return view('produits.searchProduits',compact('produits'),compact(('categories')));
    }//end of search

}
