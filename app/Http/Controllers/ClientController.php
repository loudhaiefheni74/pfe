<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Dotenv\Result\Success;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use PHPUnit\Framework\Constraint\Constraint;
use Intervention\Image\Facades\Image as Image;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $clients = Client ::paginate(10);
        return view('clients.index',compact('clients'));
    }//end of index

    public function create()
    {
        return view('clients.create');


    }//end of create


    public function store(Request $request)
    {
        $request->validate([

            'name'=>'required|min:5|max:20',
            'email'=>'required',




        ]);

        $request_data = $request->except(['image']);


        if ($request->image) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('uploads/clients_images/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

        }//end of if


        $client =Client::create($request_data);











        session()->flash('message',$client->name.(' added successfully'));
        return redirect()->route('clients.index');

    }//end of store


    public function edit(Client $client)
    {
        return view('clients.edit',compact('client'));
    }//end of edit

    public function update(Request $request , Client $client)
    {
        $request->validate([

            'name'=>'required',


        ]);
        $client->name=$request->input('name');
        $client->address=$request->input('address');
        $client->city=$request->input('city');
        $client->date_naiss=$request->input('date_naiss');
        $client->phone=$request->input('phone');
        $client->alternate_phone=$request->input('alternate_phone');






        $requestData = $request->except('email');


        $client->Update();




            $request->session()->flash('message',$client->name.(' Updated successfully'));




        return      redirect()->route('clients.index');


    }//end of update




    public function destroy(Request $request,Client $client)
    {

            $client->delete();
            session()->flash('message',$client->name.(' Deleted successfully'));

            return redirect()->route('clients.index');

    }

    public function searchClients()
    {
        $search_text = $_GET['query'];
        $clients=Client::where('name','LIKE','%'.$search_text.'%')->latest()->paginate(1);

        return view('clients.searchClients',compact('clients'));
    }//end of search

}
