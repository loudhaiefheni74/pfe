<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    protected $guarded = [];
    protected $appends=['image_path','profit_percent'];

    public function getImagePathAttribute()
    {
        return asset('uploads/produits_images/'.$this->image);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
     public function getProfitPercentAttribute()
    {
        $profit = $this->sale_price - $this->purchase_price;
        $profit_percent= $profit * 100 / $this->purchase_price;
        return number_format($profit_percent,2);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class,'produit_order');
    }
}
