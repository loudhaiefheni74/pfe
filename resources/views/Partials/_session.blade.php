@if (session()->has('message'))

    <script>
        new Noty({
            type: 'success',
            layout: 'topRight',
            text: {{session()->get('message')},
            timeout: 2000,
            killer: true
        }).show();
    </script>

@endif



