


<!DOCTYPE html>
<html lang="en">
    @include('auth.include.header')
<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
                @if (session()->has('status'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Success!</strong> {{session()->get('status')}}!
                  </div>

                 @endif

				<div class="login100-pic js-tilt" data-tilt>

					<img src="{{asset('loginpage/images/img-01.png')}}" alt="IMG">

                </div>






                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

					<span class="login100-form-title">
						Password Reset
					</span>

                        <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">

                        <input id="email" type="email" class="input100 @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>

						@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                         @enderror

                        <span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

                    <div class="container-login100-form-btn">
						<button  type="submit" class="login100-form-btn">
							{{ __('Send Password Reset Link') }}
						</button>
					</div>




                    <div  class="text-center p-t-12">



                        @if (Route::has('password.request'))
                            <a class="txt2" href="{{ route('password.request') }}">
                                <span class="txt1">

                                </span>
                                <a class="txt2" href="{{ route('password.request') }}">

                                </a>
                            </a>
                        @endif

                </div>

                <div class="text-center p-t-136">

                </div>




					</div>
				</form>
			</div>
		</div>
	</div>

<script>
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 3000);
</script>
    @include('auth.include.footer')

</body>
</html>






























































{{--



@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}
