<!-- Required Jquery -->
<script type="text/javascript" src="{{asset('dashbordpage/js/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashbordpage/js/jquery-ui/jquery-ui.min.js ')}}"></script>
<script type="text/javascript" src="{{asset('dashbordpage/js/popper.js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashbordpage/js/bootstrap/js/bootstrap.min.js ')}}"></script>
<script type="text/javascript" src="{{asset('dashbordpage/pages/widget/excanvas.js ')}}"></script>
<!-- waves js -->
<script src="{{asset('dashbordpage/pages/waves/js/waves.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{asset('dashbordpage/js/jquery-slimscroll/jquery.slimscroll.js')}} "></script>
<!-- jquery number-->
<script src="{{asset('dashbordpage/js/jquery/jquery.number.min.js')}}"></script>

<!-- modernizr js -->
<script type="text/javascript" src="{{asset('dashbordpage/js/modernizr/modernizr.js ')}}"></script>
<!-- slimscroll js -->
<script type="text/javascript" src="{{asset('dashbordpage/js/SmoothScroll.js')}}"></script>
<script src="{{asset('dashbordpage/js/jquery.mCustomScrollbar.concat.min.js ')}}"></script>
<!-- Chart js -->
<script type="text/javascript" src="{{asset('dashbordpage/js/chart.js/Chart.js')}}"></script>
<!-- amchart js -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="{{asset('dashbordpage/pages/widget/amchart/gauge.js')}}"></script>
<script src="{{asset('dashbordpage/pages/widget/amchart/serial.js')}}"></script>
<script src="{{asset('dashbordpage/pages/widget/amchart/light.js')}}"></script>
<script src="{{asset('dashbordpage/pages/widget/amchart/pie.min.js')}}"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<!-- menu js -->
<script src="{{asset('dashbordpage/js/pcoded.min.js')}}"></script>
<script src="{{asset('dashbordpage/js/vertical-layout.min.js ')}}"></script>
<!-- custom js -->
<script type="text/javascript" src="{{asset('dashbordpage/pages/dashboard/custom-dashboard.js')}}"></script>
<script type="text/javascript" src="{{asset('dashbordpage/js/script.js')}} "></script>
<script src="{{asset('dashbordpage/js/custom/order.js')}}"></script>
<script src="{{asset('dashbordpage/js/custom/image_preview.js')}}"></script>




