@if (auth()->user()->hasRole('responsable_vente'))
<!DOCTYPE html>
<html lang="en">

    @include('auth.include2.header')

  <body>
  <!-- Pre-loader start -->
  <div class="theme-loader">
      <div class="loader-track">
          <div class="preloader-wrapper">
              <div class="spinner-layer spinner-blue">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
              <div class="spinner-layer spinner-red">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>

              <div class="spinner-layer spinner-yellow">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>

              <div class="spinner-layer spinner-green">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Pre-loader end -->
  <div id="pcoded" class="pcoded">
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">
          <nav class="navbar header-navbar pcoded-header">
              <div class="navbar-wrapper">
                  <div class="navbar-logo">
                      <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                          <i class="ti-menu"></i>
                      </a>
                      <div class="mobile-search waves-effect waves-light">
                          <div class="header-search">
                              <div class="main-search morphsearch-search">
                                  <div class="input-group">
                                      <span class="input-group-addon search-close"><i class="ti-close"></i></span>
                                      <input type="text" class="form-control" placeholder="Enter Keyword">
                                      <span class="input-group-addon search-btn"><i class="ti-search"></i></span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <a href="{{route('home')}}">
                          <img class="img-fluid" src="{{asset('dashbordpage/images/logo.png')}}" alt="Theme-Logo" />
                      </a>
                      <a class="mobile-options waves-effect waves-light">
                          <i class="ti-more"></i>
                      </a>
                  </div>

                  <div class="navbar-container container-fluid">
                      <ul class="nav-left">
                          <li>
                              <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                          </li>

                          <li>
                              <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                  <i class="ti-fullscreen"></i>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav-right">

                          <li class="user-profile header-notification">
                              <a href="#!" class="waves-effect waves-light">
                                  <img src="{{Auth::user()->image_path}}" class="img-radius" alt="User-Profile-Image">
                                  <span> {{ Auth::user()->name }}</span>
                                  <i class="ti-angle-down"></i>
                              </a>
                              <ul class="show-notification profile-notification">

                                  <li class="waves-effect waves-light">
                                      <a href="{{ route('users.profile')}}">
                                          <i class="ti-user"></i> Profile
                                      </a>
                                  </li>

                                  @guest
                                  <li class="waves-effect waves-light">
                                      <a href="{{ route('login') }}">
                                           Logout
                                      </a>
                                  </li>
                                  @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            @if (auth()->user()->hasRole('admin'))
                                    <li>
                                    <a href="{{ route('users.index')}}"><i class="fa fa-users"></i>Team Member</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('users.index')}}"><i class="fa fa-address-card"></i>Clients</a>
                                        </li>
                            @endif
                            @if (auth()->user()->hasRole('responsable_vente'))
                                    <li>
                                    <a href="{{ route('users.index')}}"><i class="fa fa-address-card"></i>Clients</a>
                                    </li>
                            @endif
                                 <li class="nav-item dropdown">

                                    <a  href="{{ route('logout') }}"  onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                        <i class="ti-layout-sidebar-left"></i> Log Out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>

                                </li>
                        @endguest
                              </ul>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>

          <div class="pcoded-main-container">
              <div class="pcoded-wrapper">
                  <nav class="pcoded-navbar">
                      <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                      <div class="pcoded-inner-navbar main-menu">
                          <div class="">
                              <div class="main-menu-header">
                                  <img class="img-80 img-radius" src="{{Auth::user()->image_path}}" alt="User-Profile-Image">
                                  <div class="user-details">
                                      <span id="more-details"> {{ Auth::user()->name }}<i class="fa fa-caret-down"></i></span>
                                  </div>
                              </div>

                              <div class="main-menu-content">
                                  <ul>
                                      <li class="more-details">
                                          <a href="{{ route('users.profile')}}"><i class="ti-user"></i>View Profile</a>

                                          <a href="auth-normal-sign-in.html"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                      </li>
                                  </ul>
                              </div>
                          </div>

                          <div class="pcoded-navigation-label" data-i18n="nav.category.navigation"></div>
                          <ul class="pcoded-item pcoded-left-item">
                            <li class="pcoded">
                                <a href="{{route('home')}}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Dashboard</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu ">


                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="fa fa-address-card"></i></span>
                                    <span class="pcoded-mtext"  data-i18n="nav.basic-components.main">Clients</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class=" ">
                                        <a href="{{route('clients.index')}}"class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">show</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{route('clients.create')}}"class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Add</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{route('clients.index')}}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Edit</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{route('clients.index')}}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Delete</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>


                                </ul>

                            </li>
                            <li class="">
                                <a href="{{route('categories.index')}}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Categories</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{route('produits.index')}}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="fa fa-shopping-cart"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Products</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="{{route('clients.index')}}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-dropbox-alt"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Orders</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>


                        </ul>
                        {{--
                        <div class="pcoded-navigation-label" data-i18n="nav.category.forms">Forms &amp; Tables</div>
                          <ul class="pcoded-item pcoded-left-item">
                              <li>
                                  <a href="form-elements-component.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Form Components</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li>
                                  <a href="bs-basic-table.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Basic Table</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>

                          </ul>

                          <div class="pcoded-navigation-label" data-i18n="nav.category.forms">Chart &amp; Maps</div>
                          <ul class="pcoded-item pcoded-left-item">
                              <li>
                                  <a href="chart.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Chart</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li>
                                  <a href="map-google.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Maps</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li class="pcoded-hasmenu">
                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i></span>
                                      <span class="pcoded-mtext"  data-i18n="nav.basic-components.main">Pages</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                  <ul class="pcoded-submenu">
                                      <li class=" ">
                                          <a href="auth-normal-sign-in.html" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Login</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                      <li class=" ">
                                          <a href="auth-sign-up.html" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Register</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                      <li class=" ">
                                          <a href="sample-page.html" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Sample Page</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                  </ul>
                              </li>

                          </ul>

                          <div class="pcoded-navigation-label" data-i18n="nav.category.other">Other</div>
                          <ul class="pcoded-item pcoded-left-item">
                              <li class="pcoded-hasmenu ">
                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-direction-alt"></i><b>M</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.menu-levels.main">Menu Levels</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                  <ul class="pcoded-submenu">
                                      <li class="">
                                          <a href="javascript:void(0)" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-21">Menu Level 2.1</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                      <li class="pcoded-hasmenu ">
                                          <a href="javascript:void(0)" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-direction-alt"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-22.main">Menu Level 2.2</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                          <ul class="pcoded-submenu">
                                              <li class="">
                                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                                      <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                      <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-22.menu-level-31">Menu Level 3.1</span>
                                                      <span class="pcoded-mcaret"></span>
                                                  </a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li class="">
                                          <a href="javascript:void(0)" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-23">Menu Level 2.3</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>

                                  </ul>
                              </li>
                          </ul>
                          --}}
                      </div>
                  </nav>
                  <div class="pcoded-content">
                    <!-- Page-header start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-8">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10">Produits</h5>
                                        <p class="m-b-0">Welcome to Magasin Général</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">  <a href="index.html"> <i class="fa fa-home"></i> </a></li>
                                        <li class="breadcrumb-item" ><a href="{{route('home')}}">Dashboard</a> </li>
                                        <li class="breadcrumb-item" ><a  href="{{route('produits.index')}}">Produits</a> </li>
                                        <li class="breadcrumb-item"><a href="#!">Add</a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page-header end -->
                      <div class="pcoded-inner-content">
                          <!-- Main-body start -->
                          <div class="main-body">
                              <div class="page-wrapper">
                                  <!-- Page-body start -->
                                  <div class="page-body">
                                      <div class="row">




                                          <div class="col-md-6">
                                              <div class="card">
                                                  <div class="card-header">
                                                      <h5>Produits </h5>

                                                      <div class="card-header-right">
                                                          <ul class="list-unstyled card-option">
                                                              <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                              <li><i class="fa fa-window-maximize full-card"></i></li>
                                                              <li><i class="fa fa-minus minimize-card"></i></li>
                                                              <li><i class="fa fa-refresh reload-card"></i></li>
                                                              <li><i class="fa fa-trash close-card"></i></li>
                                                          </ul>
                                                      </div>
                                                      <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                                                  </div>
                                                  <div class="card-block">
                                                      <form action="{{ route('orders.store', $client->id) }}" method="post">

                                                          {{ csrf_field() }}
                                                          {{ method_field('post') }}



                                                          <table class="table table-hover">
                                                              <thead>
                                                              <tr>
                                                                  <th>Produits</th>
                                                                  <th>Quantity</th>
                                                                  <th>Price</th>
                                                              </tr>
                                                              </thead>

                                                              <tbody class="order-list">


                                                              </tbody>

                                                          </table><!-- end of table -->

                                                          <h4>Total : <span class="total-price">0</span></h4>

                                                          <button class="btn btn-primary btn-block disabled" id="add-order-form-btn"><i class="fa fa-plus"></i>Add Order</button>

                                                      </form>



                                                          </div>
                                                          @if ($client->orders->count() > 0)

                                                          <div class="col-md-6">
                                                              <div class="card">
                                                                  <div class="card-header">
                                                                      <h5> <small>Previous Order{{ $orders->total() }}</small></h5>

                                                                      <div class="card-header-right">
                                                                          <ul class="list-unstyled card-option">
                                                                              <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                                              <li><i class="fa fa-window-maximize full-card"></i></li>
                                                                              <li><i class="fa fa-minus minimize-card"></i></li>
                                                                              <li><i class="fa fa-refresh reload-card"></i></li>
                                                                              <li><i class="fa fa-trash close-card"></i></li>
                                                                          </ul>
                                                                      </div>
                                                                      <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                                                                  </div>

                                                                  <h3 class="box-title" style="margin-bottom: 10px">@lang('site.previous_orders')

                                                                  </h3>

                                                              <!-- end of box header -->

                                                              <div class="card-block">

                                                                  @foreach ($orders as $order)

                                                                      <div class="panel-group">

                                                                          <div class="panel panel-success">

                                                                              <div class="panel-heading">
                                                                                  <h4 class="panel-title">
                                                                                      <a data-toggle="collapse" href="#{{ $order->created_at->format('d-m-Y-s') }}">{{ $order->created_at->toFormattedDateString() }}</a>
                                                                                  </h4>
                                                                              </div>

                                                                              <div id="{{ $order->created_at->format('d-m-Y-s') }}" class="panel-collapse collapse">

                                                                                  <div class="panel-body">

                                                                                      <ul class="list-group">
                                                                                          @foreach ($order->produits as $produit)
                                                                                              <li class="list-group-item">{{ $produit->name }}</li>
                                                                                          @endforeach
                                                                                      </ul>

                                                                                  </div><!-- end of panel body -->

                                                                              </div><!-- end of panel collapse -->

                                                                          </div><!-- end of panel primary -->

                                                                      </div><!-- end of panel group -->

                                                                  @endforeach

                                                                  {{ $orders->links() }}

                                                              </div><!-- end of box body -->

                                                          </div><!-- end of box -->
                                                      </div><!-- end of box -->
                                                      <!-- end of box -->
                                                  <!-- end of box -->

                                                      @endif




                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="card">
                                                  <div class="card-header">
                                                      <h5>Categories</h5>

                                                      <div class="card-header-right">
                                                          <ul class="list-unstyled card-option">
                                                              <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                              <li><i class="fa fa-window-maximize full-card"></i></li>
                                                              <li><i class="fa fa-minus minimize-card"></i></li>
                                                              <li><i class="fa fa-refresh reload-card"></i></li>
                                                              <li><i class="fa fa-trash close-card"></i></li>
                                                          </ul>
                                                      </div>
                                                      <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                                                  </div>
                                                  <div class="card-block">
                                                      @foreach ($categories as $category)

                                                      <div class="panel-group">

                                                          <div class="panel panel-info">

                                                              <div class="panel-heading">
                                                                  <h4 class="panel-title">
                                                                      <a data-toggle="collapse" href="#{{ str_replace(' ', '-', $category->name) }}">{{ $category->name }}</a>
                                                                  </h4>
                                                              </div>

                                                              <div id="{{ str_replace(' ', '-', $category->name) }}" class="panel-collapse collapse">

                                                                  <div class="panel-body">

                                                                      @if ($category->produits->count() > 0)

                                                                          <table class="table table-hover">
                                                                              <tr>
                                                                                  <th>Name</th>
                                                                                  <th>Quantity</th>
                                                                                  <th>Price</th>
                                                                                  <th>Add</th>
                                                                              </tr>

                                                                              @foreach ($category->produits as $produit)
                                                                                  <tr>
                                                                                      <td>{{ $produit->name }}</td>
                                                                                      <td>{{ $produit->quantity}}</td>
                                                                                      <td>{{ number_format($produit->sale_price, 2) }}</td>
                                                                                      <td>
                                                                                          <a href=""
                                                                                          id="produit-{{ $produit->id }}"
                                                                                          data-name="{{ $produit->name }}"
                                                                                          data-id="{{ $produit->id }}"
                                                                                          data-price="{{ $produit->sale_price }}"
                                                                                          class="btn btn-success btn-sm add-produit-btn">
                                                                                              <i class="fa fa-plus"></i>
                                                                                          </a>
                                                                                      </td>
                                                                                  </tr>
                                                                              @endforeach

                                                                          </table><!-- end of table -->

                                                                      @else
                                                                      <h6>There are no records !</h6>
                                                                      @endif

                                                                  </div><!-- end of panel body -->

                                                              </div><!-- end of panel collapse -->

                                                          </div><!-- end of panel primary -->

                                                      </div><!-- end of panel group -->

                                                  @endforeach
                                                          </div>
                                          </div>

                                          <!--  project and team member end -->
                                      </div>
                                  </div>
                                  <!-- Page-body end -->
                              </div>
                              <div id="styleSelector"> </div>
                          </div>

                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
    <div class="ie-warning">
        <h1>Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
        <div class="iew-container">
            <ul class="iew-download">
                <li>
                    <a href="http://www.google.com/chrome/">
                        <img src="assets/images/browser/chrome.png" alt="Chrome">
                        <div>Chrome</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                        <img src="assets/images/browser/firefox.png" alt="Firefox">
                        <div>Firefox</div>
                    </a>
                </li>
                <li>
                    <a href="http://www.opera.com">
                        <img src="assets/images/browser/opera.png" alt="Opera">
                        <div>Opera</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.apple.com/safari/">
                        <img src="assets/images/browser/safari.png" alt="Safari">
                        <div>Safari</div>
                    </a>
                </li>
                <li>
                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="assets/images/browser/ie.png" alt="">
                        <div>IE (9 & above)</div>
                    </a>
                </li>
            </ul>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
    <![endif]-->
    <!-- Warning Section Ends -->

    @include('auth.include2.footer')
</body>

</html>

@elseif (auth()->user()->hasRole('admin'))
<!DOCTYPE html>
<html lang="en">

    @include('auth.include2.header')

  <body>
  <!-- Pre-loader start -->
  <div class="theme-loader">
      <div class="loader-track">
          <div class="preloader-wrapper">
              <div class="spinner-layer spinner-blue">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
              <div class="spinner-layer spinner-red">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>

              <div class="spinner-layer spinner-yellow">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>

              <div class="spinner-layer spinner-green">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Pre-loader end -->
  <div id="pcoded" class="pcoded">
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">
          <nav class="navbar header-navbar pcoded-header">
              <div class="navbar-wrapper">
                  <div class="navbar-logo">
                      <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                          <i class="ti-menu"></i>
                      </a>
                      <div class="mobile-search waves-effect waves-light">
                          <div class="header-search">
                              <div class="main-search morphsearch-search">
                                  <div class="input-group">
                                      <span class="input-group-addon search-close"><i class="ti-close"></i></span>
                                      <input type="text" class="form-control" placeholder="Enter Keyword">
                                      <span class="input-group-addon search-btn"><i class="ti-search"></i></span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <a href="{{route('home')}}">
                          <img class="img-fluid" src="{{asset('dashbordpage/images/logo.png')}}" alt="Theme-Logo" />
                      </a>
                      <a class="mobile-options waves-effect waves-light">
                          <i class="ti-more"></i>
                      </a>
                  </div>

                  <div class="navbar-container container-fluid">
                      <ul class="nav-left">
                          <li>
                              <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                          </li>

                          <li>
                              <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                  <i class="ti-fullscreen"></i>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav-right">

                          <li class="user-profile header-notification">
                              <a href="#!" class="waves-effect waves-light">
                                  <img src="{{Auth::user()->image_path}}" class="img-radius" alt="User-Profile-Image">
                                  <span> {{ Auth::user()->name }}</span>
                                  <i class="ti-angle-down"></i>
                              </a>
                              <ul class="show-notification profile-notification">

                                  <li class="waves-effect waves-light">
                                      <a href="{{ route('users.profile')}}">
                                          <i class="ti-user"></i> Profile
                                      </a>
                                  </li>

                                  @guest
                                  <li class="waves-effect waves-light">
                                      <a href="{{ route('login') }}">
                                           Logout
                                      </a>
                                  </li>
                                  @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            @if (auth()->user()->hasRole('admin'))
                                    <li>
                                    <a href="{{ route('users.index')}}"><i class="fa fa-users"></i>Team Member</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('users.index')}}"><i class="fa fa-address-card"></i>Clients</a>
                                        </li>
                            @endif
                            @if (auth()->user()->hasRole('responsable_vente'))
                                    <li>
                                    <a href="{{ route('users.index')}}"><i class="fa fa-address-card"></i>Clients</a>
                                    </li>
                            @endif
                                 <li class="nav-item dropdown">

                                    <a  href="{{ route('logout') }}"  onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                        <i class="ti-layout-sidebar-left"></i> Log Out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>

                                </li>
                        @endguest
                              </ul>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>

          <div class="pcoded-main-container">
              <div class="pcoded-wrapper">
                  <nav class="pcoded-navbar">
                      <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                      <div class="pcoded-inner-navbar main-menu">
                          <div class="">
                              <div class="main-menu-header">
                                  <img class="img-80 img-radius" src="{{Auth::user()->image_path}}" alt="User-Profile-Image">
                                  <div class="user-details">
                                      <span id="more-details"> {{ Auth::user()->name }}<i class="fa fa-caret-down"></i></span>
                                  </div>
                              </div>

                              <div class="main-menu-content">
                                  <ul>
                                      <li class="more-details">
                                          <a href="{{ route('users.profile')}}"><i class="ti-user"></i>View Profile</a>

                                          <a href="auth-normal-sign-in.html"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                      </li>
                                  </ul>
                              </div>
                          </div>

                          <div class="pcoded-navigation-label" data-i18n="nav.category.navigation"></div>
                          <ul class="pcoded-item pcoded-left-item">
                            <li class="pcoded">
                                <a href="{{route('home')}}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Dashboard</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu ">

                                <a href="javascript:void(0)" class="waves-effect waves-dark">
                                  <span class="pcoded-micon"><i class="fa fa-users"></i></span>
                                  <span class="pcoded-mtext"  data-i18n="nav.basic-components.main">Team Member</span>
                                  <span class="pcoded-mcaret"></span>
                              </a>
                              <ul class="pcoded-submenu">
                                <li class=" ">
                                    <a href="{{route('users.index')}}"class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">show</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                  <li class=" ">
                                      <a href="{{route('users.create')}}"class="waves-effect waves-dark">
                                          <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                          <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Add</span>
                                          <span class="pcoded-mcaret"></span>
                                      </a>
                                  </li>
                                  <li class=" ">
                                      <a href="{{route('users.index')}}" class="waves-effect waves-dark">
                                          <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                          <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Edit</span>
                                          <span class="pcoded-mcaret"></span>
                                      </a>
                                  </li>
                                  <li class=" ">
                                      <a href="{{route('users.index')}}" class="waves-effect waves-dark">
                                          <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                          <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Delete</span>
                                          <span class="pcoded-mcaret"></span>
                                      </a>
                                  </li>


                              </ul>

                          </li>

                            <li class="pcoded-hasmenu ">


                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="fa fa-address-card"></i></span>
                                    <span class="pcoded-mtext"  data-i18n="nav.basic-components.main">Clients</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class=" ">
                                        <a href="{{route('clients.index')}}"class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">show</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{route('clients.create')}}"class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Add</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{route('clients.index')}}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Edit</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{route('clients.index')}}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Delete</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>


                                </ul>

                            </li>
                            <li class="">
                                <a href="{{route('categories.index')}}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Categories</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{route('produits.index')}}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="fa fa-shopping-cart"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Products</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="{{route('clients.index')}}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-dropbox-alt"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Orders</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>


                        </ul>
                        {{--
                        <div class="pcoded-navigation-label" data-i18n="nav.category.forms">Forms &amp; Tables</div>
                          <ul class="pcoded-item pcoded-left-item">
                              <li>
                                  <a href="form-elements-component.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Form Components</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li>
                                  <a href="bs-basic-table.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Basic Table</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>

                          </ul>

                          <div class="pcoded-navigation-label" data-i18n="nav.category.forms">Chart &amp; Maps</div>
                          <ul class="pcoded-item pcoded-left-item">
                              <li>
                                  <a href="chart.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Chart</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li>
                                  <a href="map-google.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Maps</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li class="pcoded-hasmenu">
                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i></span>
                                      <span class="pcoded-mtext"  data-i18n="nav.basic-components.main">Pages</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                  <ul class="pcoded-submenu">
                                      <li class=" ">
                                          <a href="auth-normal-sign-in.html" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Login</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                      <li class=" ">
                                          <a href="auth-sign-up.html" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Register</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                      <li class=" ">
                                          <a href="sample-page.html" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Sample Page</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                  </ul>
                              </li>

                          </ul>

                          <div class="pcoded-navigation-label" data-i18n="nav.category.other">Other</div>
                          <ul class="pcoded-item pcoded-left-item">
                              <li class="pcoded-hasmenu ">
                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-direction-alt"></i><b>M</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.menu-levels.main">Menu Levels</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                  <ul class="pcoded-submenu">
                                      <li class="">
                                          <a href="javascript:void(0)" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-21">Menu Level 2.1</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                      <li class="pcoded-hasmenu ">
                                          <a href="javascript:void(0)" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-direction-alt"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-22.main">Menu Level 2.2</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                          <ul class="pcoded-submenu">
                                              <li class="">
                                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                                      <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                      <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-22.menu-level-31">Menu Level 3.1</span>
                                                      <span class="pcoded-mcaret"></span>
                                                  </a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li class="">
                                          <a href="javascript:void(0)" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-23">Menu Level 2.3</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>

                                  </ul>
                              </li>
                          </ul>
                          --}}
                      </div>
                  </nav>
                  <div class="pcoded-content">
                      <!-- Page-header start -->
                      <div class="page-header">
                          <div class="page-block">
                              <div class="row align-items-center">
                                  <div class="col-md-8">
                                      <div class="page-header-title">
                                          <h5 class="m-b-10">Produits</h5>
                                          <p class="m-b-0">Welcome to Magasin Général</p>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <ul class="breadcrumb-title">
                                          <li class="breadcrumb-item">  <a href="index.html"> <i class="fa fa-home"></i> </a></li>
                                          <li class="breadcrumb-item" ><a href="{{route('home')}}">Dashboard</a> </li>
                                          <li class="breadcrumb-item" ><a  href="{{route('produits.index')}}">Produits</a> </li>
                                          <li class="breadcrumb-item"><a href="#!">Add</a> </li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="row">




                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Produits </h5>

                                                        <div class="card-header-right">
                                                            <ul class="list-unstyled card-option">
                                                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                                                <li><i class="fa fa-minus minimize-card"></i></li>
                                                                <li><i class="fa fa-refresh reload-card"></i></li>
                                                                <li><i class="fa fa-trash close-card"></i></li>
                                                            </ul>
                                                        </div>
                                                        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                                                    </div>
                                                    <div class="card-block">
                                                        <form action="{{ route('orders.store', $client->id) }}" method="post">

                                                            {{ csrf_field() }}
                                                            {{ method_field('post') }}



                                                            <table class="table table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Produits</th>
                                                                    <th>Quantity</th>
                                                                    <th>Price</th>
                                                                </tr>
                                                                </thead>

                                                                <tbody class="order-list">


                                                                </tbody>

                                                            </table><!-- end of table -->

                                                            <h4>Total : <span class="total-price">0</span></h4>

                                                            <button class="btn btn-primary btn-block disabled" id="add-order-form-btn"><i class="fa fa-plus"></i>Add Order</button>

                                                        </form>



                                                            </div>
                                                            @if ($client->orders->count() > 0)

                                                            <div class="col-md-6">
                                                                <div class="card">
                                                                    <div class="card-header">
                                                                        <h5> <small>Previous Order{{ $orders->total() }}</small></h5>

                                                                        <div class="card-header-right">
                                                                            <ul class="list-unstyled card-option">
                                                                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                                                                <li><i class="fa fa-minus minimize-card"></i></li>
                                                                                <li><i class="fa fa-refresh reload-card"></i></li>
                                                                                <li><i class="fa fa-trash close-card"></i></li>
                                                                            </ul>
                                                                        </div>
                                                                        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                                                                    </div>

                                                                    <h3 class="box-title" style="margin-bottom: 10px">@lang('site.previous_orders')

                                                                    </h3>

                                                                <!-- end of box header -->

                                                                <div class="card-block">

                                                                    @foreach ($orders as $order)

                                                                        <div class="panel-group">

                                                                            <div class="panel panel-success">

                                                                                <div class="panel-heading">
                                                                                    <h4 class="panel-title">
                                                                                        <a data-toggle="collapse" href="#{{ $order->created_at->format('d-m-Y-s') }}">{{ $order->created_at->toFormattedDateString() }}</a>
                                                                                    </h4>
                                                                                </div>

                                                                                <div id="{{ $order->created_at->format('d-m-Y-s') }}" class="panel-collapse collapse">

                                                                                    <div class="panel-body">

                                                                                        <ul class="list-group">
                                                                                            @foreach ($order->produits as $produit)
                                                                                                <li class="list-group-item">{{ $produit->name }}</li>
                                                                                            @endforeach
                                                                                        </ul>

                                                                                    </div><!-- end of panel body -->

                                                                                </div><!-- end of panel collapse -->

                                                                            </div><!-- end of panel primary -->

                                                                        </div><!-- end of panel group -->

                                                                    @endforeach

                                                                    {{ $orders->links() }}

                                                                </div><!-- end of box body -->

                                                            </div><!-- end of box -->
                                                        </div><!-- end of box -->
                                                        <!-- end of box -->
                                                    <!-- end of box -->

                                                        @endif




                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Categories</h5>

                                                        <div class="card-header-right">
                                                            <ul class="list-unstyled card-option">
                                                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                                                <li><i class="fa fa-minus minimize-card"></i></li>
                                                                <li><i class="fa fa-refresh reload-card"></i></li>
                                                                <li><i class="fa fa-trash close-card"></i></li>
                                                            </ul>
                                                        </div>
                                                        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                                                    </div>
                                                    <div class="card-block">
                                                        @foreach ($categories as $category)

                                                        <div class="panel-group">

                                                            <div class="panel panel-info">

                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" href="#{{ str_replace(' ', '-', $category->name) }}">{{ $category->name }}</a>
                                                                    </h4>
                                                                </div>

                                                                <div id="{{ str_replace(' ', '-', $category->name) }}" class="panel-collapse collapse">

                                                                    <div class="panel-body">

                                                                        @if ($category->produits->count() > 0)

                                                                            <table class="table table-hover">
                                                                                <tr>
                                                                                    <th>Name</th>
                                                                                    <th>Quantity</th>
                                                                                    <th>Price</th>
                                                                                    <th>Add</th>
                                                                                </tr>

                                                                                @foreach ($category->produits as $produit)
                                                                                    <tr>
                                                                                        <td>{{ $produit->name }}</td>
                                                                                        <td>{{ $produit->quantity}}</td>
                                                                                        <td>{{ number_format($produit->sale_price, 2) }}</td>
                                                                                        <td>
                                                                                            <a href=""
                                                                                            id="produit-{{ $produit->id }}"
                                                                                            data-name="{{ $produit->name }}"
                                                                                            data-id="{{ $produit->id }}"
                                                                                            data-price="{{ $produit->sale_price }}"
                                                                                            class="btn btn-success btn-sm add-produit-btn">
                                                                                                <i class="fa fa-plus"></i>
                                                                                            </a>
                                                                                        </td>
                                                                                    </tr>
                                                                                @endforeach

                                                                            </table><!-- end of table -->

                                                                        @else
                                                                        <h6>There are no records !</h6>
                                                                        @endif

                                                                    </div><!-- end of panel body -->

                                                                </div><!-- end of panel collapse -->

                                                            </div><!-- end of panel primary -->

                                                        </div><!-- end of panel group -->

                                                    @endforeach
                                                            </div>
                                            </div>

                                            <!--  project and team member end -->
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
    <div class="ie-warning">
        <h1>Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
        <div class="iew-container">
            <ul class="iew-download">
                <li>
                    <a href="http://www.google.com/chrome/">
                        <img src="assets/images/browser/chrome.png" alt="Chrome">
                        <div>Chrome</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                        <img src="assets/images/browser/firefox.png" alt="Firefox">
                        <div>Firefox</div>
                    </a>
                </li>
                <li>
                    <a href="http://www.opera.com">
                        <img src="assets/images/browser/opera.png" alt="Opera">
                        <div>Opera</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.apple.com/safari/">
                        <img src="assets/images/browser/safari.png" alt="Safari">
                        <div>Safari</div>
                    </a>
                </li>
                <li>
                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="assets/images/browser/ie.png" alt="">
                        <div>IE (9 & above)</div>
                    </a>
                </li>
            </ul>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
    <![endif]-->
    <!-- Warning Section Ends -->

    @include('auth.include2.footer')
</body>

</html>

@else
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title> 404 Error Page</title>
  <link rel="stylesheet" href="{{asset('Errorpage/style.css')}}">

</head>
<body>
<!-- partial:index.partial.html -->
<body>
  <section id="not-found">

    <div class="circles">
      <p>404<br>
       <small>PAGE NOT FOUND</small>
      </p>
      <span class="circle big"></span>
      <span class="circle med"></span>
      <span class="circle small"></span>
    </div>
  </section>
 </body>
<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script><script  src="./script.js"></script>

</body>
</html>

@endif
























































