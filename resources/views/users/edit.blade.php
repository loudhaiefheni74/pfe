@if (auth()->user()->hasRole('admin'))
<!DOCTYPE html>
<html lang="en">

    @include('auth.include2.header')

  <body>
  <!-- Pre-loader start -->
  <div class="theme-loader">
      <div class="loader-track">
          <div class="preloader-wrapper">
              <div class="spinner-layer spinner-blue">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
              <div class="spinner-layer spinner-red">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>

              <div class="spinner-layer spinner-yellow">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>

              <div class="spinner-layer spinner-green">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Pre-loader end -->
  <div id="pcoded" class="pcoded">
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">
          <nav class="navbar header-navbar pcoded-header">
              <div class="navbar-wrapper">
                  <div class="navbar-logo">
                      <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                          <i class="ti-menu"></i>
                      </a>
                      <div class="mobile-search waves-effect waves-light">
                          <div class="header-search">
                              <div class="main-search morphsearch-search">
                                  <div class="input-group">
                                      <span class="input-group-addon search-close"><i class="ti-close"></i></span>
                                      <input type="text" class="form-control" placeholder="Enter Keyword">
                                      <span class="input-group-addon search-btn"><i class="ti-search"></i></span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <a href="{{route('home')}}">
                          <img class="img-fluid" src="{{asset('dashbordpage/images/logo.png')}}" alt="Theme-Logo" />
                      </a>
                      <a class="mobile-options waves-effect waves-light">
                          <i class="ti-more"></i>
                      </a>
                  </div>

                  <div class="navbar-container container-fluid">
                      <ul class="nav-left">
                          <li>
                              <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                          </li>

                          <li>
                              <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                  <i class="ti-fullscreen"></i>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav-right">

                          <li class="user-profile header-notification">
                              <a href="#!" class="waves-effect waves-light">
                                  <img src="{{Auth::user()->image_path}}" class="img-radius" alt="User-Profile-Image">
                                  <span> {{ Auth::user()->name }}</span>
                                  <i class="ti-angle-down"></i>
                              </a>
                              <ul class="show-notification profile-notification">
                                  <li class="waves-effect waves-light">

                                  </li>
                                  <li class="waves-effect waves-light">
                                      <a href="{{ route('users.profile')}}">
                                          <i class="ti-user"></i> Profile
                                      </a>
                                  </li>

                                  @guest
                                  <li class="waves-effect waves-light">
                                      <a href="{{ route('login') }}">
                                           Logout
                                      </a>
                                  </li>
                                  @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            @if (auth()->user()->hasRole('admin'))
                                    <li>
                                    <a href="{{ route('users.index')}}"><i class="fa fa-users"></i>Team Member</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('clients.index')}}"><i class="fa fa-address-card"></i>Clients</a>
                                        </li>

                            @endif
                                 <li class="nav-item dropdown">

                                    <a  href="{{ route('logout') }}"  onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                        <i class="ti-layout-sidebar-left"></i> Log Out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>

                                </li>
                        @endguest
                              </ul>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>

          <div class="pcoded-main-container">
              <div class="pcoded-wrapper">
                  <nav class="pcoded-navbar">
                      <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                      <div class="pcoded-inner-navbar main-menu">
                          <div class="">
                              <div class="main-menu-header">
                                  <img class="img-80 img-radius" src="{{Auth::user()->image_path}}" alt="User-Profile-Image">
                                  <div class="user-details">
                                      <span id="more-details"> {{ Auth::user()->name }}<i class="fa fa-caret-down"></i></span>
                                  </div>
                              </div>

                              <div class="main-menu-content">
                                  <ul>
                                      <li class="more-details">
                                          <a href="{{ route('users.profile')}}"><i class="ti-user"></i>View Profile</a>

                                          <a href="auth-normal-sign-in.html"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                      </li>
                                  </ul>
                              </div>
                          </div>

                          <div class="pcoded-navigation-label" data-i18n="nav.category.navigation"></div>
                          <ul class="pcoded-item pcoded-left-item">
                            <li class="pcoded">
                                <a href="{{route('home')}}" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Dashboard</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu active">

                                  <a href="javascript:void(0)"class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="fa fa-users"></i></span>
                                    <span class="pcoded-mtext"  data-i18n="nav.basic-components.main">Team Member</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class=" ">
                                        <a href="{{route('users.index')}}"class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">show</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{route('users.create')}}"class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Add</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{ route('users.edit',$user->id)}}"class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Edit</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{route('users.index')}}" class="waves-effect waves-dark">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Delete</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>


                                </ul>

                            </li>

                            @if (auth()->user()->hasRole('admin'))
                            <li class="pcoded-hasmenu ">


                                <a href="javascript:void(0)" class="waves-effect waves-dark">
                                  <span class="pcoded-micon"><i class="fa fa-address-card"></i></span>
                                  <span class="pcoded-mtext"  data-i18n="nav.basic-components.main">Clients</span>
                                  <span class="pcoded-mcaret"></span>
                              </a>
                              <ul class="pcoded-submenu">
                                  <li class=" ">
                                      <a href="{{route('clients.index')}}"class="waves-effect waves-dark">
                                          <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                          <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">show</span>
                                          <span class="pcoded-mcaret"></span>
                                      </a>
                                  </li>
                                  <li class=" ">
                                      <a href="{{route('clients.create')}}"class="waves-effect waves-dark">
                                          <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                          <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Add</span>
                                          <span class="pcoded-mcaret"></span>
                                      </a>
                                  </li>
                                  <li class=" ">
                                      <a href="{{route('clients.index')}}" class="waves-effect waves-dark">
                                          <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                          <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Edit</span>
                                          <span class="pcoded-mcaret"></span>
                                      </a>
                                  </li>
                                  <li class=" ">
                                      <a href="{{route('clients.index')}}" class="waves-effect waves-dark">
                                          <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                          <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Delete</span>
                                          <span class="pcoded-mcaret"></span>
                                      </a>
                                  </li>


                              </ul>

                          </li>
                          <li class="">
                            <a href="{{route('categories.index')}}" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>P</b></span>
                                <span class="pcoded-mtext" data-i18n="nav.dash.main">Categories</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('produits.index')}}" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="fa fa-shopping-cart"></i><b>P</b></span>
                                <span class="pcoded-mtext" data-i18n="nav.dash.main">Products</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('clients.index')}}" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="ti-dropbox-alt"></i><b>P</b></span>
                                <span class="pcoded-mtext" data-i18n="nav.dash.main">Orders</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                        </li>
                          @endif
                        </ul>
                        {{--
                          <div class="pcoded-navigation-label" data-i18n="nav.category.forms">Forms &amp; Tables</div>
                          <ul class="pcoded-item pcoded-left-item">
                              <li>
                                  <a href="form-elements-component.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Form Components</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li>
                                  <a href="bs-basic-table.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Basic Table</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>

                          </ul>

                          <div class="pcoded-navigation-label" data-i18n="nav.category.forms">Chart &amp; Maps</div>
                          <ul class="pcoded-item pcoded-left-item">
                              <li>
                                  <a href="chart.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Chart</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li>
                                  <a href="map-google.html" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.form-components.main">Maps</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li class="pcoded-hasmenu">
                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i></span>
                                      <span class="pcoded-mtext"  data-i18n="nav.basic-components.main">Pages</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                  <ul class="pcoded-submenu">
                                      <li class=" ">
                                          <a href="auth-normal-sign-in.html" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Login</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                      <li class=" ">
                                          <a href="auth-sign-up.html" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Register</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                      <li class=" ">
                                          <a href="sample-page.html" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.basic-components.breadcrumbs">Sample Page</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                  </ul>
                              </li>

                          </ul>

                          <div class="pcoded-navigation-label" data-i18n="nav.category.other">Other</div>
                          <ul class="pcoded-item pcoded-left-item">
                              <li class="pcoded-hasmenu ">
                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                      <span class="pcoded-micon"><i class="ti-direction-alt"></i><b>M</b></span>
                                      <span class="pcoded-mtext" data-i18n="nav.menu-levels.main">Menu Levels</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                  <ul class="pcoded-submenu">
                                      <li class="">
                                          <a href="javascript:void(0)" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-21">Menu Level 2.1</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                      <li class="pcoded-hasmenu ">
                                          <a href="javascript:void(0)" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-direction-alt"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-22.main">Menu Level 2.2</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                          <ul class="pcoded-submenu">
                                              <li class="">
                                                  <a href="javascript:void(0)" class="waves-effect waves-dark">
                                                      <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                      <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-22.menu-level-31">Menu Level 3.1</span>
                                                      <span class="pcoded-mcaret"></span>
                                                  </a>
                                              </li>
                                          </ul>
                                      </li>
                                      <li class="">
                                          <a href="javascript:void(0)" class="waves-effect waves-dark">
                                              <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                              <span class="pcoded-mtext" data-i18n="nav.menu-levels.menu-level-23">Menu Level 2.3</span>
                                              <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>

                                  </ul>
                              </li>
                          </ul>
                          --}}
                      </div>
                  </nav>
                  <div class="pcoded-content">
                      <!-- Page-header start -->
                      <div class="page-header">
                          <div class="page-block">
                              <div class="row align-items-center">
                                  <div class="col-md-8">
                                      <div class="page-header-title">
                                          <h5 class="m-b-10">Team Member</h5>
                                          <p class="m-b-0">Welcome to Magasin Général</p>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">  <a href="index.html"> <i class="fa fa-home"></i> </a></li>
                                        <li class="breadcrumb-item" class="active"><a href="{{route('home')}}">Dashboard</a> </li>
                                        <li class="breadcrumb-item"><a href="{{route('users.index')}}">Team Member</a> </li>
                                        <li class="breadcrumb-item"><a href="{{ route('users.edit',$user->id)}}">Edit</a> </li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">

                                <div class="page-wrapper">

                                    @if (session()->has('message'))
                                    <div class="alert alert-success" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Success!</strong> {{session()->get('message')}}!
                                      </div>

                                           @endif
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="cover-profile">
                                                    <div class="profile-bg-img">
                                                        <img class="profile-bg-img img-fluid" src="{{asset('dashbordpage\images\back-user.jpg')}}" alt="bg-img">
                                                        <div class="card-block user-info">
                                                            <div class="col-md-12">
                                                                <div class="media-left">
                                                                    <a href="#" class="profile-image">
                                                                        <img class="user-img img-radius" src="{{$user->image_path}}"alt="user-img">
                                                                    </a>
                                                                </div>
                                                                <div class="media-body row">
                                                                    <div class="col-lg-12">
                                                                        <div class="user-title">
                                                                            <h2>{{$user->name}}</h2>
                                                                            <span class="text-white"> {{$user->role}}</span>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-header-text">About Me</h5>
                                            <div class=" waves-effect waves-light f-right" style="display: inline-flex">
                                                <button onclick="show1()" type="button" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                &nbsp;
                                                <button onclick="show2()" type="button" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="view-info" style="" id="div1">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="general-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-xl-6">
                                                                        <div class="table-responsive">
                                                                            <table class="table m-0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <th scope="row">Full Name</th>
                                                                                        <td>{{$user->name}}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th scope="row">Email</th>
                                                                                        <td>{{$user->email}}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th scope="row">Birth Date</th>
                                                                                        <td>{{$user->date_naiss}}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th scope="row">Address</th>
                                                                                        <td>{{$user->address}}</td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                    <div class="col-lg-12 col-xl-6">
                                                                        <div class="table-responsive">
                                                                            <table class="table">
                                                                                <tbody>


                                                                                    <tr>
                                                                                        <th scope="row">Location</th>
                                                                                        <td>{{$user->city}}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th scope="row">Mobile Number </th>
                                                                                        <td>{{$user->phone}}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th scope="row">Alternate Mobile Number </th>
                                                                                        <td>{{$user->alternate_phone}}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th scope="row">Fonction </th>
                                                                                        <td>@foreach ($user->roles as $index=>$role )
                                                                                             {{ $role->display_name }} {{ $index+1 < $user->roles -> count() ? ',' : '' }}</p>
                                                                                            @endforeach</td>
                                                                                    </tr>


                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>
                                                            <!-- end of general info -->
                                                        </div>
                                                        <!-- end of col-lg-12 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of view-info -->
                                                <div class="edit-info" style="display: none;"  id="div2">
                                                    <form action="{{route('users.update',$user->id)}}"method="post">

                                                        {{ csrf_field() }}

                                                        {{ method_field('PUT') }}

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="general-info form-material">
                                                                <div class="row">
                                                                    <div class="col-lg-6 ">



                                                                        <div class="material-group">
                                                                            <div class="material-addone">
                                                                                <i class="fa fa-user"></i>
                                                                            </div>
                                                                            <div class="form-group form-primary">
                                                                                <input type="text"name="name"  class="form-control" value="{{$user->name}}" required="">
                                                                                <span class="form-bar"></span>
                                                                                <label class="float-label">Full Name</label>
                                                                            </div>
                                                                        </div>



                                                                        <div class="material-group">
                                                                            <div class="material-addone">
                                                                                <i class="fa fa-birthday-cake"></i>
                                                                            </div>
                                                                            <div class="form-group form-primary">
                                                                                <input type="date" name="date_naiss" value="{{$user->date_naiss}}"class="form-control">
                                                                                <span class="form-bar"></span>
                                                                                <label class="float-label">Select Your Birth Date</label>
                                                                            </div>
                                                                        </div>





                                                                    <div class="material-group">
                                                                        <div class="material-addone">
                                                                            <i class="fa fa-map-marker"></i>
                                                                        </div>
                                                                        <div class="form-group form-primary">
                                                                            <input type="text" name="address"  value="{{$user->address}}" class="form-control"required="">
                                                                            <span class="form-bar"></span>
                                                                            <label class="float-label">Address</label>
                                                                        </div>
                                                                    </div>
                                                                    <br>



                                                                </div>
                                                                <!-- end of table col-lg-6 -->
                                                                <div class="col-lg-6">

                                                                    <div class="material-group">
                                                                        <div class="material-addone">
                                                                            <i class="fa fa-building"></i>
                                                                        </div>
                                                                        <div class="form-group form-primary">
                                                                            <input type="text" name="city"  value="{{$user->city}}"class="form-control"required="">
                                                                            <span class="form-bar"></span>
                                                                            <label class="float-label">Location</label>

                                                                        </div>
                                                                    </div>


                                                                    <div class="material-group">
                                                                        <div class="material-addone">
                                                                            <i class="fa fa-phone"></i>
                                                                        </div>
                                                                        <div class="form-group form-primary">
                                                                            <input type="text" name="phone"  value="{{$user->phone}}"class="form-control"required="">
                                                                            <span class="form-bar"></span>
                                                                            <label class="float-label">Mobile Number</label>
                                                                        </div>
                                                                    </div>



                                                                    <div class="material-group">
                                                                        <div class="material-addone">
                                                                            <i class="fa fa-phone"></i>
                                                                        </div>
                                                                        <div class="form-group form-primary">
                                                                            <input type="text" name="alternate_phone"  value="{{$user->alternate_phone}}"class="form-control"required="">
                                                                            <span class="form-bar"></span>
                                                                            <label class="float-label">Alternate Mobile Number</label>
                                                                        </div>
                                                                    </div>


                                                                        <div class="form-group form-primary">
                                                                            <div class="col-md-10"style="display:inline-flex">

                                                                                <div class="checkbox" >
                                                                                    <label>

                                                                                        <div class="chk-option">
                                                                                            <div class="checkbox-fade fade-in-primary">
                                                                                                <label class="check-task">
                                                                                                    <input disabled class="form-control" type="checkbox" name="roles[]" value="admin" {{$user->hasRole('admin')?'checked' : ''}}>   Admin
                                                                                                    <span class="cr">
                                                                                                        <i class="cr-icon fa fa-check txt-default"></i>
                                                                                                    </span>

                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </label>

                                                                                </div>
                                                                              <div class="checkbox" >
                                                                                <label>

                                                                                    <div class="chk-option">
                                                                                        <div class="checkbox-fade fade-in-primary">
                                                                                            <label class="check-task">
                                                                                                <input type="checkbox" name="roles[]" value="magasinier" {{$user->hasRole('magasinier')?'checked' : ''}}>   Magasinier
                                                                                                <span class="cr">
                                                                                                    <i class="cr-icon fa fa-check txt-default"></i>
                                                                                                </span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </label>

                                                                                </div>

                                                                                <div class="checkbox">
                                                                                <label>

                                                                                    <div class="chk-option">
                                                                                        <div class="checkbox-fade fade-in-primary">
                                                                                            <label class="check-task">
                                                                                                <input type="checkbox" name="roles[]" value="responsable_devis" {{$user->hasRole('responsable_devis')?'checked' : ''}}>  Responsable Devis
                                                                                                <span class="cr">
                                                                                                    <i class="cr-icon fa fa-check txt-default"></i>
                                                                                                </span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </label>

                                                                             </div>
                                                                                <div class="checkbox">
                                                                                <label>

                                                                                    <div class="chk-option">
                                                                                        <div class="checkbox-fade fade-in-primary">
                                                                                            <label class="check-task">
                                                                                                <input type="checkbox" name="roles[]" value="responsable_vente" {{$user->hasRole('responsable_vente')?'checked' : ''}}> Responsable Vente
                                                                                                <span class="cr">
                                                                                                    <i class="cr-icon fa fa-check txt-default"></i>
                                                                                                </span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </label>

                                                                             </div>
                                                                                <div class="checkbox">
                                                                                <label>

                                                                                    <div class="chk-option">
                                                                                        <div class="checkbox-fade fade-in-primary">
                                                                                            <label class="check-task">
                                                                                                <input type="checkbox" name="roles[]" value="financier" {{$user->hasRole('financier')?'checked' : ''}}>  Financier
                                                                                                <span class="cr">
                                                                                                    <i class="cr-icon fa fa-check txt-default"></i>

                                                                                                </span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </label>

                                                                                </div>

                                                                                 </div>
                                                                            <span class="form-bar"></span>

                                                                        </div>










                                                                </div>
                                                                <!-- end of table col-lg-6 -->
                                                            </div>
                                                            <!-- end of row -->
                                                            <div class="text-center">
                                                                <button  class="btn btn-primary waves-effect waves-light m-r-20">  Save</button>


                                                            </div>
                                                        </div>
                                                        <!-- end of edit info -->
                                                    </div>
                                                    <!-- end of col-lg-12 -->
                                                </div>
                                                <!-- end of row -->
                                            </form>
                                            </div>
                                            <!-- end of edit-info -->
                                        </div>
                                        <!-- end of card-block -->
                                    </div>




                                    </div>
                                    <!-- Page-body end -->
                                </div>








                                {{-- *********************************************************************--}}
                                {{-- *********************************************************************--}}
                                {{-- *********************************************************************--}}
                                {{-- *********************************************************************--}}
                                {{-- *********************************************************************--}}
                                {{-- *********************************************************************--}}
                                {{-- *********************************************************************--}}
                                {{-- *********************************************************************



                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="row">




                                            <!--  project and team member start -->
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Edit User {{$user->name}}</h5>


                                                        <div class="card-header-right">
                                                            <ul class="list-unstyled card-option">
                                                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                                                <li><i class="fa fa-minus minimize-card"></i></li>
                                                                <li><i class="fa fa-refresh reload-card"></i></li>
                                                                <li><i class="fa fa-trash close-card"></i></li>
                                                            </ul>
                                                        </div>
                                                        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                                                    </div>
                                                    <div class="card-block">

                                                        <form action="{{route('users.update',$user->id)}}"method="post">

                                                            {{ csrf_field() }}

                                                            {{ method_field('PUT') }}




                                                        <div class="col-md-12"style="display: inline-flex">
                                                            <div class="col-md-4 ">
                                                                <div class="col-sm-10">


                                                                    <img src="{{$user->image_path}}" class="img-radius" alt="User-Profile-Image">


                                                                </div>

                                                            </div>



                                                        </div>


                                                        <div class="col-md-8">
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Name</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text"name="name" value="{{$user->name}}" class="form-control">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Email</label>
                                                                <div class="col-sm-10">
                                                                    <input type="email"name="email"readonly value="{{$user->email}}" class="form-control">
                                                                </div>
                                                            </div>

                                                        </div>


                                                            <div class="col-md-8">
                                                                <div class="form-group row">
                                                                    <label class="col-sm-2 col-form-label">Address</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" name="address" value="{{$user->address}}" class="form-control">
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <div class="col-md-8">
                                                                <div class="form-group row">
                                                                    <label class="col-sm-2 col-form-label">City</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" name="city"value="{{$user->city}}"class="form-control">
                                                                    </div>
                                                                </div>

                                                            </div>



                                                            <div class="col-md-8">
                                                                <div class="form-group row">
                                                                    <label class="col-sm-2 col-form-label">Date of Birth</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="date" name="date_naiss"value="{{$user->date_naiss}}" class="form-control">
                                                                    </div>
                                                                </div>

                                                            </div>



                                                            <div  class="col-md-12"style="display: inline-flex">
                                                                <div class="col-md-6">
                                                                    <div class="form-group row">
                                                                        <label class="col-sm-2 col-form-label">Phone</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="text" name="phone" value="{{$user->phone}}"class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group row">
                                                                        <label class="col-sm-2 col-form-label">Alternate Phone</label>
                                                                        <div class="col-sm-10">

                                                                            <input type="text" name="alternate_phone"value="{{$user->alternate_phone}}"class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-10"style="display:inline-flex">

                                                                <label class="col-sm-2 col-form-label">Fonction</label>
                                                                <div class="checkbox" >
                                                                    <label>

                                                                        <div class="chk-option">
                                                                            <div class="checkbox-fade fade-in-primary">
                                                                                <label class="check-task">
                                                                                    <input disabled class="form-control" type="checkbox" name="roles[]" value="admin" {{$user->hasRole('admin')?'checked' : ''}}>   Admin
                                                                                    <span class="cr">
                                                                                        <i class="cr-icon fa fa-check txt-default"></i>
                                                                                    </span>

                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </label>

                                                                </div>
                                                              <div class="checkbox" >
                                                                <label>

                                                                    <div class="chk-option">
                                                                        <div class="checkbox-fade fade-in-primary">
                                                                            <label class="check-task">
                                                                                <input type="checkbox" name="roles[]" value="magasinier" {{$user->hasRole('magasinier')?'checked' : ''}}>   Magasinier
                                                                                <span class="cr">
                                                                                    <i class="cr-icon fa fa-check txt-default"></i>
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </label>

                                                                </div>

                                                                <div class="checkbox">
                                                                <label>

                                                                    <div class="chk-option">
                                                                        <div class="checkbox-fade fade-in-primary">
                                                                            <label class="check-task">
                                                                                <input type="checkbox" name="roles[]" value="responsable_devis" {{$user->hasRole('responsable_devis')?'checked' : ''}}>  Responsable Devis
                                                                                <span class="cr">
                                                                                    <i class="cr-icon fa fa-check txt-default"></i>
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </label>

                                                             </div>
                                                                <div class="checkbox">
                                                                <label>

                                                                    <div class="chk-option">
                                                                        <div class="checkbox-fade fade-in-primary">
                                                                            <label class="check-task">
                                                                                <input type="checkbox" name="roles[]" value="responsable_vente" {{$user->hasRole('responsable_vente')?'checked' : ''}}> Responsable Vente
                                                                                <span class="cr">
                                                                                    <i class="cr-icon fa fa-check txt-default"></i>
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </label>

                                                             </div>
                                                                <div class="checkbox">
                                                                <label>

                                                                    <div class="chk-option">
                                                                        <div class="checkbox-fade fade-in-primary">
                                                                            <label class="check-task">
                                                                                <input type="checkbox" name="roles[]" value="financier" {{$user->hasRole('financier')?'checked' : ''}}>  Financier
                                                                                <span class="cr">
                                                                                    <i class="cr-icon fa fa-check txt-default"></i>

                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </label>

                                                                </div>

                                                                 </div>

                                                            <div style="text-align: center">
                                                                <button  class="btn btn-primary btn-round waves-effect waves-light"> <i class="fa fa-edit"> </i> Update Profile</button>

                                                                </div>




                                                             </form>
                                                            </div>
                                                </div>
                                            </div>



                                            <!--********************************************************** -->
                                            <!--********************************************************** -->
                                            <!--********************************************************** -->




                                            <!--  project and team member end -->
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>--}}
                                <div id="styleSelector"> </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
    <div class="ie-warning">
        <h1>Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
        <div class="iew-container">
            <ul class="iew-download">
                <li>
                    <a href="http://www.google.com/chrome/">
                        <img src="assets/images/browser/chrome.png" alt="Chrome">
                        <div>Chrome</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                        <img src="assets/images/browser/firefox.png" alt="Firefox">
                        <div>Firefox</div>
                    </a>
                </li>
                <li>
                    <a href="http://www.opera.com">
                        <img src="assets/images/browser/opera.png" alt="Opera">
                        <div>Opera</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.apple.com/safari/">
                        <img src="assets/images/browser/safari.png" alt="Safari">
                        <div>Safari</div>
                    </a>
                </li>
                <li>
                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="assets/images/browser/ie.png" alt="">
                        <div>IE (9 & above)</div>
                    </a>
                </li>
            </ul>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
    <![endif]-->
    <!-- Warning Section Ends -->


    <script type="text/javascript">
        function show1(){
            document.getElementById('div2').style.display="block"
            document.getElementById('div1').style.display="none"
        }
        function show2(){
            document.getElementById('div1').style.display="block"
            document.getElementById('div2').style.display="none"
        }
    </script>

    @include('auth.include2.footer')
</body>

</html>
@endif



























