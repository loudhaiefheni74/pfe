<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mega Able</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                    width:100%;
                    height:100%;
                    background:linear-gradient(
                        -135deg
                        , #c5d9ef, #007bff);
                    font-family: 'Raleway', sans-serif;
                    font-weight:300;
                    margin:0;
                    padding:0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
                text-align:center;
                margin-top:40px;
                margin-bottom:-40px;
                position:relative;
                color:rgb(58, 53, 53);
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a class="txt2" href="{{ url('/home') }}">Home</a>
                    @else
                        <a  class="txt2"href="{{ route('login') }}">Login</a>


                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Mega Able
                </div>


            </div>
        </div>
    </body>
</html>
