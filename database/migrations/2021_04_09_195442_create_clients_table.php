<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('date_naiss')->default('NON DEFINI(E)');
            $table->string('city')->default('NON DEFINI(E)');
            $table->string('phone')->default('NON DEFINI(E)');
            $table->string('alternate_phone')->default('NON DEFINI(E)');
            $table->string('address')->default('NON DEFINI(E)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
