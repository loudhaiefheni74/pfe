<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('image')->default('C:\Users\HENI LDF\blog\public\uploads\user_images\1AEwTrsCjZbsZk7jSO4vFrXHMj5CRv4KIf6oazhb.jpg');
            $table->string('password');
            $table->string('date_naiss')->default('NON DEFINI(E)');
            $table->string('address')->default('NON DEFINI(E)');
            $table->string('city')->default('NON DEFINI(E)');
            $table->string('phone')->default('NON DEFINI(E)');
            $table->string('alternate_phone')->default('NON DEFINI(E)');



            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
