<?php

use Illuminate\Database\Seeder;

class ProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $produits = ['pro one'];

        foreach ($produits as $produit) {

            \App\Produit::create([
                'category_id' => 1,
                'name' => $produit,
                'image'=>'public/uploads/produits_images/prod.png',
                'purchase_price' => 100,
                'sale_price' => 150,
                'quantity' => 100,
            ]);

        }//end of foreach

    }
}
