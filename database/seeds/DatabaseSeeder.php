<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *s
     * @return void
     */
    public function run()
    {
         $this->call(LaratrustSeeder::class);

         $this->call(UserTableSeeder::class);

         $this->call(CategoriesTableSeeder::class);

         $this->call(ProduitsTableSeeder::class);

         $this->call(ClientsTableSeeder::class);

    }
}
