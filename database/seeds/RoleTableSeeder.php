<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin=App\Models\Role::create([
            'name'=>'admin',
            'display_name'=> 'admin',
            'description'=>'Can do anything in the project',

        ]);
        $user=App\Models\Role::create([
            'name'=>'user',
            'display_name'=> 'user',
            'description'=>'Can do specific',

        ]);
    }
}
