<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $clients = ['Ahmed'];

        foreach ($clients as $client) {

            \App\Client::create([
               'name' => $client,
               'phone' => '25484841',
               'alternate_phone' => '23556214',
                'email'=>'ahmed@gmail.com',
                'date_naiss' => '28/05/1997',
                'address' => 'Avenue Omar ibn Khatab',
                'city' => 'Bizerte',

            ]);

        }//end of foreach

    }
}
