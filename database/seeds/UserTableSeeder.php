<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=App\User::create([
            'name'=>'Admin',
            'email'=>'admin@app.com',
            'password'=>bcrypt('secret'),
        ]);
        $user->attachRole('admin');

    }
}
