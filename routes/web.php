<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Users Routes
Route::resource('users', 'UserController');
Route::get('/profile', [ 'as' => 'users.profile', 'uses' => 'UserController@myprofile']);
Route::post('/my-profile-update','UserController@profileupdate');
Route::get('/search','UserController@search');


//Categories Routes
Route::resource('categories', 'CategoryController')->except(['show']);
Route::get('/searchCategories','CategoryController@searchCategories');

//Clients Routes
Route::resource('clients', 'ClientController')->except(['show']);
Route::get('/searchClients','ClientController@searchClients');


//Produits Routes
Route::resource('produits', 'ProduitController')->except(['show']);
Route::get('/searchProduits','ProduitController@searchProduits');


//Orders Routes
Route::resource('orders', 'OrderController')->except(['show']);
Route::get('/searchOrders','OrderController@searchProduits');

//Ordersindex Route
Route::resource('ordersindex', 'OrderindexController')->except(['show']);











