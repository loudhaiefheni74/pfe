<?php

return [
    'role_structure' => [
        'admin' => [
            'users' => 'c,r,u,d',
            'clients' => 'c,r,u,d',
            'produits' => 'c,r,u,d',
            'orders' => 'c,r,u,d',
            'categories' => 'c,r,u,d',


        ],
        'financier'=>[

        ],
        'magasinier'=>[

        ],
        'responsable_devis'=>[

        ],
        'responsable_vente'=>[
            'clients' => 'c,r,u,d',
            'produits' => 'c,r,u,d',
            'orders' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
        ],



    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
